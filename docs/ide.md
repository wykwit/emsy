# STM32CubeIDE

We started off with familiarizing ourselves with the dedicated development environment provided by the manufacturer - STM32CubeIDE.
We created some simple programs for our evaluation board and tested the operation of components that would be used in the project.

![STM32CubeMX editor window preview.](img/CubeIDE-hellomx.png)

Using the STM32CubeMX code generator, we created a basic project template using FreeRTOS, which we then adapted to our needs.

# PlatformIO

To facilitate work on the project, we decided to use PlatformIO[^pio], another development environment. The proposed project structure differs from that generated by STM32CubeMX, but we managed to adapt the project and successfully compile it using PlatformIO.

We had to add support for our prototype board by placing its description in JSON format in the appropriate directory (`~/.platformio/platforms/ststm32/boards`). This description is located in our repository in the `platform` subdirectory.

Compilation is done by running the `pio run` command. Integration of the environment with the VSCode editor allows convenient work with the project, including debugging.

You can read more about building the project in `docs/build.md`.

[^pio]: https://platformio.org/
