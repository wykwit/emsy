# Wishlist

1. Smart button combo would allow the user to enter settings mode
    where one can change all parameters of each tone associated with a button.

2. Automated testing with PlatformIO. ([docs](https://docs.platformio.org/en/latest/advanced/unit-testing/index.html))

3. Move envelope back to the tone and make every single one adjustable separately or in a group.
