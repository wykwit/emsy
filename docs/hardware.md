# Hardware Platform

The first issue to address was the selection of the hardware platform. Since our topic revolves around music, we defined our basic requirements for the prototype board we would like to use in the project:

1. Audio codec, DAC converter, and 3.5mm jack output -- for easy output of generated sound.
2. Processor with a built-in FPU.
3. STM32 platform, following the project supervisor's suggestion.

The first prototype board we proposed was the STM32F407G-DISC1, previously known as STM32F4DISCOVERY[^1]. After consulting with the project supervisor, we managed to borrow the STM3241G-EVAL evaluation board[^2], which is also based on the ARM Cortex-M4F processor with identical specifications, but the board itself has additional peripherals and GPIO outputs. The evaluation board meets all our requirements, so we were satisfied with the choice of this a hardware platform.

After we got the project to a working state on the evaluation board, we then attempted a port to STM32F407G-DISC1.

[^1]: https://www.st.com/en/evaluation-tools/stm32f4discovery.html
[^2]: https://www.st.com/en/evaluation-tools/stm3241g-eval.html
