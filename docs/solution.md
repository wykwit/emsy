# Solution Architecture

The solution consists of several components:

1. Generating signals (tones) with appropriate parameters.
2. Reading input.
3. Mixing and modulating multiple signals (tones).
4. Constantly playing the generated sound.

## User Interface

In the end, we created a simple user interface consisting of several basic buttons connected to GPIO ports. Each button activates the playback of tones assigned to it. All tones are modulated using an ADSR envelope, which is configured when the device is turned on.

Unfortunately, due to the limitations of the very basic user interface, it is not possible to change the frequency of tones assigned to buttons or envelope parameters during device operation. Considering the architecture of our solution, adding such functionality or including more buttons should not pose a significant challenge. In our case, the limitation was the lack of access to additional electronic components during execution of this task, which also resulted in a small number of connected buttons in our demo.

## Concurrency

Launching the device initializes and configures hardware components, then FreeRTOS takes over control. The entry point of the program is the `src/main.c` file. Individual threads are implemented in the `src/freertos.c` file. The header file `include/freertos.h` contains dependencies used by threads, and the configuration file is `include/FreeRTOSConfig.h`. Threads do not block each other.

The main thread is responsible for blinking the LED, illustrating the absence of significant delays in the system. Besides that, there is a thread responsible for reading the input and regularly checking the button states, as well as a thread responsible for communication with the audio codec.

Interrupts are generated when any button is pressed or released, and during the transfer of the generated sound to the output using DMA. General interrupt handling is implemented in the `src/stm32f4xx_it.c` file and usually involves calling the HAL function, which then calls the appropriate callback. These functions are implemented in files specific to the components for which individual interrupts are relevant.

The interrupt handling function marks buttons for checking, and then the input reading thread checks the button value, appropriately changes its state, and optionally calls the button handling function suitable for transitioning to a specific state. This approach achieves "debouncing". By checking the button state stored in memory before reading the values of the GPIO ports, we introduce only a small delay, which should not be noticeable to the user.

## Design Patterns

In the project, we attempted to apply elements of an object-oriented style. Modules consist of `.c` code files containing the implementation and `.h` header files declaring structures, public functions, and dependencies for a given module. The fundamental flaw is that not all module data has been separated into proper context structures. Additionally, private functions were not correctly marked with the `static` modifier. Fully implementing an object-oriented style would require further refactoring.

In the tone module, there is support for determining the frequency and generating tones, mixing them together, and handling the output device. This broad range of responsibilities suggests that the tone module could be further divided into smaller modules. For example, the audio codec handling could use the adapter pattern and be placed in a separate module.

## Optimizations

During the project implementation, we introduced optimizations necessary to achieve smooth system operation. Due to the project's nature, any shortcomings were immediately noticeable in the device's performance.

Project optimization focused on improving speed while maintaining reasonable memory consumption. These elements have the most significant impact on the noticeable system performance and pave the way for further project development. Other criteria such as energy consumption or reliability were not considered to the same extent.

A measurement of the execution time of a critical function filling the played sound buffer was conducted for the current version and the one before introducing additional buffers for individual tones. In the previous version, the buffer was five times longer, and filling it required generating values for each tone. In the current version, the output buffer is shorter, but each tone has its own buffer with values generated during device configuration. Memory consumption has increased, but execution time has significantly decreased.

| tag  | output buffer | execution time | polyphony | total memory consumption |
|:----:|----------:|---------:|----------:|--------------:|
| v0.2 |      8820 | 11705354 | 2-3 tones | 37784 (28.8%) |
| v0.3 |      1764 |   757668 | 20 tones  | 94180 (71.9%) |
| HEAD |      1764 |   757668 | 20+ tones | 58900 (44.9%) |

![Current version of the `TONE_Generate` function.](img/measure-cycles-v0.3.png)

![Previous version of the `TONE_Generate` function.](img/measure-cycles-v0.2.png)

## Static Analysis

The project source files were checked for compliance with the MISRA-C standard using the "cppcheck" tool, and unfortunately, they did not meet all requirements.

Static code analysis of the project can be invoked using the `pio check` command. The scan resulted in many false positives; nevertheless, achieving compliance with the standard at this stage of the project would certainly not be a straightforward task.
