# Build instructions

## Prerequisites

We use [PlatformIO](https://platformio.org/) as our build system.
You will need "platformio-core" installed if you want to follow these instructions.

## Build

Our build is defined in the `platformio.ini` file.

To build the project simply use:
```
$ pio run
```

This might fail when you run it for the first time since you don't have the platform set up yet.
Read on for instructions on how to fix that, then try again.

To change the optimization level see [this forum thread](https://community.platformio.org/t/compiler-optimization-levels-for-stm32/4206).
We've defined additional environment with a `build_type = debug` which should also compile the project with no optimizations.

## Platform

Default Platform Manager should install "ststm32" platform when you "pio run" for the first time.
We use a custom board definition, so the build process won't proceed unless you install it manually.

```
$ cp platform/eval_f417igh6.json ~/.platformio/platforms/ststm32/boards/
```

When you "pio run" again, the default Tool Manager should install all the required tools, including:

 - gcc compiler for the ARM Cortex-M4F
 - stm32cubef4 framework with STM32F4 HAL
 - ldscripts for STM32
 - STM32F4 BSP, along with the variant for our board

and then the project should build successfully.

## Flashing

You can simply use:
```
$ pio run -t upload
```

If you want to use st-link utilities, you can also:
```
$ st-flash write .pio/build/eval_f417igh6/firmware.bin 0x8000000
```

## More documentation

PlatformIO is a really cool build system - flexible and quite easy to understand.

Their documentation is available here: <https://docs.platformio.org/en/latest/>

Their forum is also very helpful: <https://community.platformio.org/>
