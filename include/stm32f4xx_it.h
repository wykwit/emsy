#ifndef __STM32F4xx_IT_H
#define __STM32F4xx_IT_H

#include "errors.h"
#include "stm32f4xx_hal.h"

void NMI_Handler(void);
void HardFault_Handler(void);
void MemManage_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);
void DebugMon_Handler(void);
void EXTI0_IRQHandler(void);
void EXTI1_IRQHandler(void);
void EXTI2_IRQHandler(void);
void EXTI4_IRQHandler(void);
void EXTI9_5_IRQHandler(void);
void EXTI15_10_IRQHandler(void);
void DMA1_Stream4_IRQHandler(void);
void TIM1_UP_TIM10_IRQHandler(void);

#endif /* __STM32F4xx_IT_H */
