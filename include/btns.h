#ifndef __BTNS_H__
#define __BTNS_H__

#include "cmsis_os.h"
#include "errors.h"
#include "tone.h"

enum BTN { A, B, X, Y, Z, LEN };
enum BTN_S {OFF, ON, D_FOR_OFF, D_FOR_ON} BTN_State[LEN];

void BTNS_Init(osThreadId_t);
int32_t BTNS_Read(uint16_t);
void BTNS_Pressed(uint16_t);
void BTNS_Released(uint16_t);

#endif /* __BTNS_H__ */
