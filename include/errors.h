#ifndef __ERRORS_H__
#define __ERRORS_H__

#include "stm32f4xx_hal.h"

#define LED3_Pin        GPIO_PIN_9
#define LED3_GPIO_Port  GPIOI
#define LED4_Pin        GPIO_PIN_7
#define LED4_GPIO_Port  GPIOC

void Error_Handler(void);
void Error_Led(int pin, int state);

#endif /* __ERRORS_H__ */
