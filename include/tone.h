#ifndef __TONE_H__
#define __TONE_H__

#include "errors.h"
#include "fint.h"
#include "math.h"
#include "stm324xg_eval_audio.h"

// defines

#define TONE_BUF_LEN 1764
#define TONE_LIST_MAX 20

// types

typedef enum {
    SILENT,
    PLAYING,
    RELEASE,
} tone_state_t;

typedef struct {
    // state of the tone
    tone_state_t state;

    // tone frequency
    float_t freq;

    // private values
    // used during generation
    uint32_t _cycle;
    uint32_t _tick;
    uint32_t _tick_r;

    int16_t _buf[TONE_BUF_LEN/2];
} tone_t;

// pseudo allocations
tone_t* TONE_New(void);
void TONE_Free(tone_t *);

// properties
void TONE_Freq(tone_t *self, int16_t steps);
void TONE_Envelope(uint32_t a, uint32_t d, float_t s, uint32_t r);

// methods
void TONE_Push(tone_t *);
void TONE_Release(tone_t *);

// output device
uint8_t TONE_Init(void);
uint8_t TONE_Play(void);
uint8_t TONE_Stop(void);

#endif /* __TONE_H__ */
