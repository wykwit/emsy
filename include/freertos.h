#ifndef __FREERTOS_H__
#define __FREERTOS_H__

#include "FreeRTOS.h"
#include "btns.h"
#include "cmsis_os.h"
#include "errors.h"
#include "task.h"
#include "tone.h"

void FREERTOS_Init(void);

#endif /* __FREERTOS_H__ */
