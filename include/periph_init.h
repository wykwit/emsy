#ifndef __PERIPH_INIT_H__
#define __PERIPH_INIT_H__

#include "errors.h"
#include "stm32f4xx_hal.h"

void PERIPH_SystemClock_Config(void);
void PERIPH_GPIO_Init(void);
void PERIPH_TIM6_Init(void);
void PERIPH_I2C1_Init(void);
void PERIPH_I2S2_Init(void);

#endif /* __PERIPH_INIT_H__ */
