#ifndef __FINT_H__
#define __FINT_H__

#include <stdint.h>

int32_t fint32(float x);
uint32_t fuint32(float x);
int16_t fint16(float x);
uint16_t fuint16(float x);

#endif /* __FINT_H__ */
