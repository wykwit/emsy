#include "stm32f4xx_hal.h"

#define MEASURE_CYCLES_START() \
    int C, S1, S2; \
    SysTick->LOAD = 0xFFFFFF; \
    SysTick->VAL = 0; \
    SysTick->CTRL = 0x05; \
    S1 = SysTick->VAL;

#define MEASURE_CYCLES_STOP() \
    S2 = SysTick->VAL; \
    C = S1 - S2 - 2;
