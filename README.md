# emsy

emsy - Embedded synth with ADSR envelope.

This project was conceived for the "Embedded Systems Software Architecture" course at the Warsaw University of Technology.

Check out `docs/` for more detailed information:

- [build instructions](docs/build.md)
- [hardware platform](docs/hardware.md)
- [development environment](docs/ide.md)
- [solution architecture](docs/solution.md)
- [wishlist for future](docs/wishlist.md)

## license

MIT unless stated otherwise.
