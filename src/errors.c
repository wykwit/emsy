#include "errors.h"

// set LED pin to a given state {0, 1} to signal Error
void Error_Led(int pin, int state)
{
    switch (pin) {
        case 3:
            HAL_GPIO_WritePin(LED3_GPIO_Port, LED3_Pin, state);
            break;
        case 4:
            HAL_GPIO_WritePin(LED4_GPIO_Port, LED4_Pin, state);
            break;
    }
}

// default handler will enable LED3 to signal Error and hang the system
void Error_Handler(void)
{
    Error_Led(3, 1);
    __disable_irq();
    while (1) {}
}
