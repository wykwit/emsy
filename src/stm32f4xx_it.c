#include "stm32f4xx_it.h"

I2S_HandleTypeDef haudio_i2s;
TIM_HandleTypeDef htim1;

/***********************************************************/
/* Cortex-M4 Processor Interruption and Exception Handlers */
/***********************************************************/

/**
 * @brief This function handles Non maskable interrupt.
 */
void NMI_Handler(void) {}

/**
 * @brief This function handles Hard fault interrupt.
 */
void HardFault_Handler(void) {}

/**
 * @brief This function handles Memory management fault.
 */
void MemManage_Handler(void) {}

/**
 * @brief This function handles Pre-fetch fault, memory access fault.
 */
void BusFault_Handler(void) {}

/**
 * @brief This function handles Undefined instruction or illegal state.
 */
void UsageFault_Handler(void) {}

/**
 * @brief This function handles Debug monitor.
 */
void DebugMon_Handler(void) {}

/*
 * STM32F4xx Peripheral Interrupt Handlers
 * Add here the Interrupt Handlers for the used peripherals.
 * For the available peripheral interrupt handler names,
 * please refer to the startup file (startup_stm32f4xx.s).
 */

void EXTI0_IRQHandler(void)     { HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0); }
void EXTI1_IRQHandler(void)     { HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1); }
void EXTI2_IRQHandler(void)     { HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2); }
void EXTI4_IRQHandler(void)     { HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_4); }
void EXTI9_5_IRQHandler(void)   { HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_5); }

void DMA1_Stream4_IRQHandler(void)
{
    HAL_DMA_IRQHandler(haudio_i2s.hdmatx);
}

void AUDIO_I2Sx_DMAx_IRQHandler(void)
{
    Error_Led(3, 1);
    HAL_DMA_IRQHandler(haudio_i2s.hdmatx);
}

void TIM1_UP_TIM10_IRQHandler(void)
{
    HAL_TIM_IRQHandler(&htim1);
}
