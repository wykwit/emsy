#include "main.h"

int main(void)
{
    // reset peripherals, initialize flash and systick
    HAL_Init();

    // configure the system clock
    PERIPH_SystemClock_Config();

    // initialize peripherals
    PERIPH_GPIO_Init();
    PERIPH_TIM6_Init();
    PERIPH_I2C1_Init();
    PERIPH_I2S2_Init();

    // initialize rtos
    osKernelInitialize();
    FREERTOS_Init();

    // start
    osKernelStart();

    // we should never get here as control is now taken by the scheduler
    while (1) {}
}
