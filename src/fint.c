#include "fint.h"

uint32_t union_cast(float x)
{
	union {
		float f;
		uint32_t u;
	} r;

	r.f = x;
	return r.u;
}

int32_t fint24(float x)
{
	// extracting bits
	uint32_t u_x = union_cast(x);
	uint32_t sign = u_x >> 31;
	uint32_t exponent = (u_x >> 23) & 0xFF;
	uint32_t fraction = (u_x & 0x7FFFFF) + 0x800000;

	// special case: 0
	if (u_x == 0)
		return 0;

	// special case: inf, -inf
	if (exponent == 0xFF)
		return (sign == 0) ? INT32_MAX : INT32_MIN;

	// test highest bit
	if (exponent & 0x80) {
		// multiplication
		fraction <<= (exponent - 127);
	} else {
		// division
		fraction >>= (127 - exponent);
	}

	return (int32_t) fraction * (sign ? -1 : 1);
}

// convert float fractional part to int32_t in the full range
int32_t fint32(float x)
{
	// scale up to 32 bits
	return (fint24(x) << 8);
}

// convert float fractional part to uint32_t in the full range
uint32_t fuint32(float x)
{
	int32_t r = fint32(x);
	if (r < 0)
		r ^= 0xFFFFFFFF;
	return r << 1;
}

// convert float fractional part to int16_t in the full range
int16_t fint16(float x)
{
	// scale down to 16 bits
	return (fint24(x) >> 8);
}

// convert float fractional part to uint16_t in the full range
uint16_t fuint16(float x)
{
	int32_t r = fint24(x) >> 7;
	if (r < 0)
		r ^= 0xFFFF;
	return r;
}

