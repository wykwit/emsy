#include "tone.h"


// == VALUES ==

// output device configuration
const uint8_t VOL = 100;
const uint8_t dampen = 5;

// tone constants
const float_t _ET_BASE = 1.05946309436f;
const float_t _FREQ_BASE = (440.0f * 3.141592653589793f * 2.0f) / 44100.0f;

// output buffer
uint16_t TONE_BUF[TONE_BUF_LEN] = {0};

// list of tones to generate and mix
tone_t TONE_LIST[TONE_LIST_MAX];

// adsr envelope for the tone
struct {
    uint32_t a; // tick duration of attack
    uint32_t d; // tick duration of decay
    float_t  s; // %value of sustain
    uint32_t r; // tick duration of release
} envelope;


// == PSEUDO ALLOCATIONS ==

// return pointer to unused note
tone_t* TONE_New(void)
{
    for (uint32_t i = 0; i < TONE_LIST_MAX; i++) {
        if (TONE_LIST[i].freq == 0)
            return &TONE_LIST[i];
    }
    return NULL;
}

// mark the note as unused
void TONE_Free(tone_t *t)
{
    t->state = SILENT;
    t->freq = 0;
}


// == PROPERTIES ==

/**
 * @brief Frequency of a note away from reference by the number of halfsteps.
 *
 * For   0 steps you get A_4 at 440 Hz.  That is our default reference point.
 * For -36 steps you get A_1 at 55 Hz.   That is probably the lowest you want.
 * For  48 steps you get A_8 at 7040 Hz. That is probably the highest you want.
 *
 * @param steps Number of halfsteps in relation to the reference (A_4 note).
 * @return Frequency of a note.
 */
void TONE_Freq(tone_t *self, int16_t steps)
{
    self->freq = _FREQ_BASE * powf(_ET_BASE, steps);
    self->_cycle = 0;

    float_t _freq = 0;
    int16_t val = 0;
    int16_t prev = 0;
    uint32_t tick = 0;

    // generating
    for (uint_fast32_t i = 0; i < TONE_BUF_LEN; i += 2) {
        _freq += self->freq;
        val = fint16(sinf(_freq)) >> dampen;

        // sign changed
        if (((val) ^ (prev)) & 0x8000) {
            // reset value on a cycle
            if (self->_cycle) {
                self->_cycle = tick;
                return;
            }
            self->_cycle += 1;
        }

        prev = val;
        tick += 1;

        self->_buf[tick] = val;
    }
}

void TONE_Envelope(uint32_t a, uint32_t d, float_t s, uint32_t r)
{
    envelope.a = a;
    envelope.d = d + a;
    envelope.s = s;
    envelope.r = r;
}


// == METHODS ==

void TONE_Push(tone_t *self)
{
    self->_tick = 0;
    self->_tick_r = 0;
    self->state = PLAYING;
}

void TONE_Release(tone_t *self)
{
    self->_tick_r = self->_tick;
    self->state = RELEASE;
}


// == OUTPUT BUFFER ==

float_t TONE_Amp(tone_t *self)
{
    uint32_t t = self->_tick;
    uint32_t tr = t - (self->_tick_r);

    switch(self->state) {
        case RELEASE: {
            if (tr > envelope.r) {
                self->state = SILENT;
                return 0.0f;
            }
            return  envelope.s * ((float_t) (envelope.r - tr) / (float_t) envelope.r);
        }

        case PLAYING:
        default: {
            if (t < envelope.a) {
                return (float_t) t / (float_t) envelope.a;
            } else if (t < envelope.d)
                return envelope.s + (1.0f - envelope.s) * ((float_t) (envelope.d - t) / (float_t) (envelope.d - envelope.a));
            else
                return envelope.s;
        }
    }
}

void TONE_Generate(uint32_t start, uint32_t end)
{
    int16_t val;
    int32_t t_i;
    tone_t *t;

    for (uint_fast32_t i = start; i < end; i += 2) {
        val = 0;

        // mixing all the tones
        for (t_i = 0; t_i < TONE_LIST_MAX; t_i += 1) {
            t = &TONE_LIST[t_i];           

            if (t->state == SILENT)
                continue;

            val += t->_buf[t->_tick % t->_cycle] * TONE_Amp(t);
            t->_tick += 1;
        }

        TONE_BUF[i] = val;
        TONE_BUF[i+1] = val;
    }
}


// == OUTPUT DEVICE ==

// interface

uint8_t TONE_Init(void)
{
    return BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_HEADPHONE, VOL, I2S_AUDIOFREQ_44K);
}

uint8_t TONE_Play(void)
{
    TONE_Generate(0, TONE_BUF_LEN);
    return BSP_AUDIO_OUT_Play(TONE_BUF, TONE_BUF_LEN);
}

uint8_t TONE_Stop(void)
{
    return BSP_AUDIO_OUT_Stop(CODEC_PDWN_SW);
}

// private functions

void TONE_Shift(uint32_t start, uint32_t end)
{
    BSP_AUDIO_OUT_ChangeBuffer(TONE_BUF + start, end);
}

// callbacks implementation

void BSP_AUDIO_OUT_TransferComplete_CallBack(void)
{
    // set playback to the first half
    TONE_Shift(0, TONE_BUF_LEN);
    // generate values for the second half
    TONE_Generate(TONE_BUF_LEN/2, TONE_BUF_LEN);
}

void BSP_AUDIO_OUT_HalfTransfer_CallBack(void)
{
    // generate values for the first half
    TONE_Generate(0, TONE_BUF_LEN/2);
}

void BSP_AUDIO_OUT_Error_CallBack(void)
{
    Error_Handler();
    TONE_Stop();
}
