#include "freertos.h"


// task configuration

osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
    .name = "defaultTask",
    .stack_size = 128 * 4,
    .priority = (osPriority_t) osPriorityNormal,
};

osThreadId_t inputTaskHandle;
const osThreadAttr_t inputTask_attributes = {
    .name = "inputTask",
    .stack_size = 128 * 4,
    .priority = (osPriority_t) osPriorityLow,
};

osThreadId_t toneTaskHandle;
const osThreadAttr_t toneTask_attributes = {
    .name = "toneTask",
    .stack_size = 128 * 4,
    .priority = (osPriority_t) osPriorityLow,
};


// task implementation

void StartDefaultTask(void *argument)
{
    for(;;) {
        // blink LED4 to signal that a parallel thread is working correctly
        Error_Led(4, 1);
        osDelay(500);
        Error_Led(4, 0);
        osDelay(500);
    }
}

void StartInputTask(void *argument)
{
    BTNS_Init(inputTaskHandle);

    for(;;) {
        osDelay(30);
        for (int b = A; b < LEN; b++) {
            if (BTN_State[b] == D_FOR_ON) {
                if (BTNS_Read(b)) {
                    BTNS_Pressed(b);
                    BTN_State[b] = ON;
                } else {
                    BTN_State[b] = OFF;
                }
            } else if (BTN_State[b] == D_FOR_OFF) {
                if (BTNS_Read(b)) {
                    BTN_State[b] = ON;
                } else {
                    BTNS_Released(b);
                    BTN_State[b] = OFF;
                }
            }
        }
    }
}

void StartToneTask(void *argument)
{
    // enable output
    TONE_Init();
    TONE_Play();

    for(;;) {
        osDelay(1000);
    }

    // stop output
    TONE_Stop();
}


// initialization of FreeRTOS objects
void FREERTOS_Init(void)
{
    // mutexes
    // semaphores
    // timers
    // queues

    // threads
    defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);
    inputTaskHandle = osThreadNew(StartInputTask, NULL, &inputTask_attributes);
    toneTaskHandle = osThreadNew(StartToneTask, NULL, &toneTask_attributes);

    // events
}
