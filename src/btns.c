#include "btns.h"


// == DEFINES ==

#define MS 44
#define ADD_NOTE(b, i, s) { \
    BTN_Tones[b][i] = TONE_New();     \
    TONE_Freq(BTN_Tones[b][i], s);    \
}

// == VALUES ==

osThreadId_t INPUT_THREAD = NULL;

tone_t *BTN_Tones[LEN][4];


// == IMPLEMENTATION ==

void BTNS_Init(osThreadId_t t)
{
    INPUT_THREAD = t;

    TONE_Envelope(400*MS, 240*MS, 0.32f, 400*MS);

    // Am
    ADD_NOTE(A, 0, -12); // A
    ADD_NOTE(A, 1, -9);  // C
    ADD_NOTE(A, 2, -5);  // E
    ADD_NOTE(A, 3, 0);   // A

    // Dm
    ADD_NOTE(B, 0, -7);  // D
    ADD_NOTE(B, 1, -4);  // F
    ADD_NOTE(B, 2, 0);   // A
    ADD_NOTE(B, 3, 5);   // D

    // Em
    ADD_NOTE(X, 0, -5); // E
    ADD_NOTE(X, 1, -2); // G
    ADD_NOTE(X, 2, 2);  // B
    ADD_NOTE(X, 3, 7);  // E

    // F
    ADD_NOTE(Y, 0, -4); // F
    ADD_NOTE(Y, 1, 0);  // A
    ADD_NOTE(Y, 2, 3);  // C
    ADD_NOTE(Y, 3, 8);  // F

    // G
    ADD_NOTE(Z, 0, -2); // G
    ADD_NOTE(Z, 1, 2);  // B
    ADD_NOTE(Z, 2, 5);  // D
    ADD_NOTE(Z, 3, 10); // G
}

uint16_t BTNS_From(uint16_t pin)
{
    switch (pin) {
        case GPIO_PIN_0:  return A;
        case GPIO_PIN_1:  return B;
        case GPIO_PIN_2:  return Z;
        case GPIO_PIN_4:  return X;
        case GPIO_PIN_5:  return Y;

        default: return LEN;
    }
}

int32_t BTNS_Read(uint16_t b)
{
    switch (b) {
        case A: return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_0);
        case B: return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1);
        case X: return HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_4);
        case Y: return HAL_GPIO_ReadPin(GPIOC, GPIO_PIN_5);
        case Z: return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2);

        default: return -1;
    }
}

void BTNS_Pressed(uint16_t b)
{
    TONE_Push(BTN_Tones[b][0]);
    TONE_Push(BTN_Tones[b][1]);
    TONE_Push(BTN_Tones[b][2]);
    TONE_Push(BTN_Tones[b][3]);
    BTN_State[b] = 1;
}

void BTNS_Released(uint16_t b)
{
    TONE_Release(BTN_Tones[b][0]);
    TONE_Release(BTN_Tones[b][1]);
    TONE_Release(BTN_Tones[b][2]);
    TONE_Release(BTN_Tones[b][3]);
    BTN_State[b] = 0;
}

// callback implementation

void HAL_GPIO_EXTI_Callback(uint16_t pin)
{
    uint16_t b = BTNS_From(pin);
    switch (BTN_State[b]) {
        case OFF:
            BTN_State[b] = D_FOR_ON;
            break;

        case ON:
            BTN_State[b] = D_FOR_OFF;
            break;

        default:
            break;
    }
}
